import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import "./Components.css";

export default class GetCoinFlipsComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let flipsArray = [];
    this.props.individualFlips.forEach(element => {
      let flip = (
        <Row>
          <Col>{element.flipNumber}</Col>
          <Col>{element.side}</Col>
          <Col>
            <img src={element.coinImage} />{" "}
          </Col>
        </Row>
      );
      flipsArray.push(flip);
    });

    return (
      <div>
        <Container>
          <Row>
            <Col>Outcome</Col>
            <Col>Coin Image</Col>
            <Col>Outcome Percentage</Col>
          </Row>
          <Row>
            <Col>{this.props.outcome}</Col>
            <Col>
              <img src={this.props.coinImage} />{" "}
            </Col>
            <Col>{this.props.outcomePercentage}</Col>
          </Row>
          <Row>
            <Col />
            <Col>Individual Flips</Col>
            <Col />
          </Row>
          <Row>
            <Col>Flip Number</Col>
            <Col>Outcome</Col>
            <Col>Coin Image</Col>
          </Row>
          {flipsArray}
        </Container>
      </div>
    );
  }
}
