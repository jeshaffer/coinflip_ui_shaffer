import React, { Component } from "react";
import "./App.css";
import { Button, Alert } from "reactstrap";
import { getCoinFlips } from "./coinflipService";
import GetCoinFlipsComponent from "./GetCoinFlipsComponent";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      outcome: "",
      coinImage: "",
      outcomePercentage: "",
      individualFlips: [],
      id: 1,
      isLoaded: false,
      visible: true
    };
    this.onDismiss = this.onDismiss.bind(this);
    this.setId = this.setId.bind(this);
    this.getCoinFlipsFromHeroku = this.getCoinFlipsFromHeroku.bind(this);
  }

  getCoinFlipsFromHeroku() {
    getCoinFlips(this.state.id).then(coinflip => {
      this.setState({
        outcome: coinflip.outcome,
        coinImage: coinflip.coinImage,
        outcomePercentage: coinflip.outcomePercentage,
        individualFlips: coinflip.individualFlips,
        isLoaded: true
      });
    });
  }

  setId(event) {
    this.setState({ id: event.target.value });
  }

  onDismiss() {
    this.setState({ visible: false });
  }

  render() {
    let alert =
      this.state.id < 1 || this.state.id > 29 || this.state.id % 2 == 0 ? (
        <Alert
          color="danger"
          isOpen={this.state.visible}
          toggle={this.onDismiss}
        >
          Enter an odd number between 1-29.
        </Alert>
      ) : (
        ""
      );

    let coinFlipOutput = this.state.isLoaded ? (
      <GetCoinFlipsComponent
        outcome={this.state.outcome}
        coinImage={this.state.coinImage}
        outcomePercentage={this.state.outcomePercentage}
        individualFlips={this.state.individualFlips}
      />
    ) : (
      ""
    );

    return (
      <div className="App">
        <header className="App-header">
          <p id="title">Coin Flip</p>
          <div id="input">
            <input onChange={this.setId} placeholder="# times to flip coin" />
            <p />
            <Button color="secondary" onClick={this.getCoinFlipsFromHeroku}>
              Flip Coin
            </Button>
          </div>
          <div id="alert">{alert}</div>
          {coinFlipOutput}
        </header>
      </div>
    );
  }
}

export default App;
