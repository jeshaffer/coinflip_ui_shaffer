"use strict";
import rp from "request-promise";

export function getCoinFlips(id) {
  return rp({
    method: "GET",
    uri: "https://jeshaffer-coinflip.herokuapp.com/coinflip/" + id,
    json: true
  });
}
